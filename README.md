# Minecraft Server Manager

A shell script to manage Minecraft servers.

Currently this mostly uses screens so that is a dependency.

## Examples

### Crontab

```
@reboot /home/user/minecraft-server-manager/minecraft-server-manager.sh --new minecraft-001
@reboot sleep 2; /home/user/minecraft-server-manager/minecraft-server-manager.sh --start minecraft-001
0 12 * * * /home/user/minecraft-server-manager/minecraft-server-manager.sh --backup minecraft-001
0 6 * * * /home/user/minecraft-server-manager/minecraft-server-manager.sh --restart minecraft-001
0 7 * * * /home/user/minecraft-server-manager/minecraft-server-manager.sh --backup-clean minecraft-001
```

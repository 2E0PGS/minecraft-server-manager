#!/bin/bash

# Minecraft Server Manager.
# Copyright (C) <2019>  <Peter Stevenson> (2E0PGS)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

root_folder="/home/$LOGNAME/minecraft-server-manager"
dropbox_folder="/home/$LOGNAME/Dropbox"

command_input_processor() {
	command_input_prefix="$1"
	server_name="$2"
	command_input_suffix="$3"

	if [ "$command_input_prefix" == "--help" ]; then
		list_help_commands
	elif [ "$command_input_prefix" == "--raw" ]; then
		check_input_srvname_cmdsuffix "$server_name" "$command_input_suffix"
		raw "$server_name" "$command_input_suffix"
	elif [ "$command_input_prefix" == "--backup" ]; then
		check_input_srvname "$server_name"
		backup_worlds "$server_name"
	elif [ "$command_input_prefix" == "--backup-clean" ]; then
		check_input_srvname "$server_name"
		cleanup_old_backups "$server_name"
	elif [ "$command_input_prefix" == "--restart" ]; then
		check_input_srvname "$server_name"
		restart_server "$server_name"
	elif [ "$command_input_prefix" == "--stop" ]; then
		check_input_srvname "$server_name"
		stop_server "$server_name"
	elif [ "$command_input_prefix" == "--start" ]; then
		check_input_srvname "$server_name"
		start_server "$server_name"
	elif [ "$command_input_prefix" == "--announce" ]; then
		check_input_srvname_cmdsuffix "$server_name" "$command_input_suffix"
		announce "$server_name" "$command_input_suffix"
	elif [ "$command_input_prefix" == "--save-on" ]; then
		check_input_srvname "$server_name"
		save_on "$server_name"
	elif [ "$command_input_prefix" == "--save-off" ]; then
		check_input_srvname "$server_name"
		save_off "$server_name"
	elif [ "$command_input_prefix" == "--dropbox-on" ]; then
		check_input_srvname "$server_name"
		dropbox_backup_on "$server_name"
	elif [ "$command_input_prefix" == "--dropbox-off" ]; then
		check_input_srvname "$server_name"
		dropbox_backup_off "$server_name"
	elif [ "$command_input_prefix" == "--new" ]; then
		check_input_srvname "$server_name"
		create_new_managed_server_instance "$server_name"
	elif [ "$command_input_prefix" == "--destory" ]; then
		check_input_srvname "$server_name"
		destroy_managed_server_instance "$server_name"
	elif [ "$command_input_prefix" == "--list" ]; then
		list_managed_server_instances
	elif [ "$command_input_prefix" == "" ]; then
		list_help_commands
	else
		echo "ERROR Unknown command."
		exit 1
	fi
}

check_input_srvname() {
	if [ "$1" == "" ]; then
		echo "ERROR No server name specified."
		exit 1
	fi
}

check_input_srvname_cmdsuffix() {
	check_input_srvname "$1"
	if [ "$2" == "" ]; then
		echo "ERROR No command suffix specified."
		exit 1
	fi
}

list_help_commands() {
	echo "Minecraft Server Manager."
	echo "Written by 2E0PGS."
	echo "List of commands:"
	echo "Flag: --help                        Description: Lists the available commands."
	echo "Flag: --raw <server> <command>      Description: Sends any command directly to the server."
	echo "Flag: --backup <server>             Description: Take archival backup now."
	echo "Flag: --backup-clean <server>       Description: Clean up backups older than 7 days."
	echo "Flag: --restart <server>            Description: Restarts the server."
	echo "Flag: --stop <server>               Description: Stops the server."
	echo "Flag: --start <server>              Description: Starts the server."
	echo "Flag: --announce <server> <message> Description: Announces a message to the server."
	echo "Flag: --save-on <server>            Description: Enables automatic world saving."
	echo "Flag: --save-off <server>           Description: Disables automatic world saving."
	echo "Flag: --dropbox-on <server>         Description: Enables dropbox syncing of all backups."
	echo "Flag: --dropbox-off <server>        Description: Disables dropbox syncing of all backups."
	echo "Flag: --new <server>                Description: Creates new managed server instance."
	echo "Flag: --destory <server>            Description: Stops server and kills screen session."
	echo "Flag: --list                        Description: List managed server instances."
}

send_command_to_screen() {
	screen -S "$1" -p 0 -X stuff "$2^M"
}

announce() {
	send_command_to_screen "$1" "say [MSN] $2"
}

raw() {
	send_command_to_screen "$1" "$2"
}

backup_worlds() {
	announce "$1" "Backing up world..."
	save_off "$1"
	sleep 5 #  Give it some time to disable saves.
	tar_compress_world "$1"
	save_on "$1"
	announce "$1" "Backup complete."
}

cleanup_old_backups() {
	find "$root_folder"/"$1"/*-world-backup.tar.gz -maxdepth 0 -mtime +7 -delete
}

tar_compress_world() {
	# tar compress the world to a known location.
	# TODO: Make a backups folder. Backup everthing besides that.
	cd "$root_folder"
	cd "$1"
	tar czf $(date "+%Y-%m-%d-%H-%M-%S")-world-backup.tar.gz world*
}

save_off() {
	send_command_to_screen "$1" "save-off"
}

save_on() {
	send_command_to_screen "$1" "save-on"
}

create_new_managed_server_instance() {
	screen -dmS "$1" # Runs screen as detached daemon.
	if [ ! -d "$root_folder" ]; then
		echo "No root folder found at: "$root_folder""
		echo "Creating one."
		mkdir "$root_folder"
	fi
	if [ ! -d "$root_folder"/"$1" ]; then
		echo "No server folder found at: "$root_folder"/"$1""
		echo "Creating one."
		mkdir "$root_folder"/"$1"
	fi
	send_command_to_screen "$1" "cd "$root_folder"/"$1""
}

list_managed_server_instances() {
	screen -list | grep minecraft
}

destroy_managed_server_instance() {
	stop_server
	send_command_to_screen "$1" "exit"
}

dropbox_backup_on() {
	if [ -d "$dropbox_folder" ]; then
		echo "Found dropbox folder."
		if [ ! -d ""$dropbox_folder"/"$1"" ]; then
			echo "No exsisting link... creating..."
			ln -s ""$root_folder"/"$1"" "$dropbox_folder" # Symlink.
		else
			echo "Link already exsits."
		fi
	else
		echo "ERROR No dropbox folder found."
	fi
}

dropbox_backup_off() {
	if [ -d "$dropbox_folder" ]; then
		echo "Found dropbox folder."
		if [ -d ""$dropbox_folder"/"$1"" ]; then
			echo "Found exsisting link... Removing..."
			rm ""$dropbox_folder"/"$1"" # Remove symlink.
		else
			echo "No exsisting link."
		fi
	else
		echo "ERROR No dropbox folder found."
	fi
}

stop_server() {
	send_command_to_screen "$1" "stop" # Send command to JAR with server id and command.
}

start_server() {
	# TODO: Don't use cd anywhere in the script. Reference paths fully or relative to MSM.
	# This starts a server differently if it finds a start script present.
	# Useful for spigot servers so we get crash recovery.
	cd "$root_folder"
	cd "$1"
	if [ -f "start.sh" ]; then
		echo "Found start.sh. If using spigot the server should crash recover."
		send_command_to_screen "$1" "./start.sh"
	else
		echo "Couldn't find start.sh. If using spigot consider a start.sh file for crash recovery."
		send_command_to_screen "$1" "java -Xmx4096M -Xms4096M -jar server.jar nogui"
	fi
}

restart_server() {
	announce "$1" "Server restarting in 3 mins.."
	sleep 60
	announce "$1" "Server restarting in 2 mins.."
	sleep 60
	announce "$1" "Server restarting in 1 mins.."
	sleep 60
	announce "$1" "Server restarting now..."
	stop_server "$1"
	sleep 10 # TODO: This should probably check if server is stopped via stdout.
	start_server "$1"
}

command_input_processor $@ # Call after everything else. Otherwise we can't find functions.
